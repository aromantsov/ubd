<?php
// Heading
$_['heading_title']    			= 'Настройка Sms';

// Text
$_['text_module']      			= 'Модули';
$_['text_success']    			= 'Настройки модуля успешно обновлены!';
$_['text_edit']        			= 'Редактирование модуля';

// Entry
$_['entry_sms_gatename']        = 'Шлюз SMS:';
$_['entry_sms_to']              = 'Номер телефона получателя';
$_['entry_sms_from']            = 'Псевдоним отправителя';
$_['entry_sms_message']         = 'Текст сообщения';
$_['entry_sms_gate_username']   = 'Логин на SMS шлюз';
$_['entry_sms_gate_password']   = 'Пароль на SMS шлюз';
$_['entry_sms_alert']           = 'Включить SMS уведомления';
$_['entry_sms_copy']            = 'Дополнительные номера';

$_['help_sms_to']               = 'В международном формате, только цифры 380xxxxxxxxxx';
$_['help_sms_from']             = 'Не более 11 символов, либо номер телефона до 15 цифр';
$_['help_sms_copy']             = 'Указывать через запятую, в международном формате, без разделителей 380xxxxxxxxxx';
$_['help_sms_message']          = 'Можно использовать теги:<br/>{ID} - номер заказа<br/>{DATE} - дата заказа<br/>{TIME} - время заказа<br/>{SUM} - сумма заказа<br/>{PHONE} - телефон клиента';

// Error
$_['error_permission'] 			= 'У Вас нет прав для управления этим модулем!';