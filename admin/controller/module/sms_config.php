<?php
class ControllerModuleSmsConfig extends Controller {
	private $error = array();

	public function index(){
		$this->load->language('module/sms_config');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('localisation/language');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('sms_config', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->cache->delete('sms_config');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'],
				'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['entry_sms_gatename'] = $this->language->get('entry_sms_gatename');
		$data['entry_sms_to'] = $this->language->get('entry_sms_to');
		$data['entry_sms_from'] = $this->language->get('entry_sms_from');
		$data['entry_sms_message'] = $this->language->get('entry_sms_message');
		$data['entry_sms_gate_username'] = $this->language->get('entry_sms_gate_username');
		$data['entry_sms_gate_password'] = $this->language->get('entry_sms_gate_password');
		$data['entry_sms_alert'] = $this->language->get('entry_sms_alert');
		$data['entry_sms_copy'] = $this->language->get('entry_sms_copy');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['help_sms_to'] = $this->language->get('help_sms_to');
		$data['help_sms_from'] = $this->language->get('help_sms_from');
		$data['help_sms_copy'] = $this->language->get('help_sms_copy');
		$data['help_sms_message'] = $this->language->get('help_sms_message');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = [];

		$data['breadcrumbs'][] = [
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		];

		$data['breadcrumbs'][] = [
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		];

		$data['breadcrumbs'][] = [
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/sms_config', 'token=' . $this->session->data['token'], 'SSL')
		];

		$data['action'] = $this->url->link('module/sms_config', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$data['token'] = $this->session->data['token'];

		$data['sms_gatenames'] = [];

		$files = glob(DIR_SYSTEM . 'smsgate/*.php');

		foreach ($files as $file) {
			$data['sms_gatenames'][] = basename($file, '.php');
		}

		if (isset($this->request->post['sms_config_alert'])) {
			$data['sms_config_alert'] = $this->request->post['sms_config_alert'];
		} else {
			$data['sms_config_alert'] = $this->config->get('sms_config_alert');
		}

		if (isset($this->request->post['sms_config_gatename'])) {
			$data['sms_config_gatename'] = $this->request->post['sms_config_gatename'];
		} else {
			$data['sms_config_gatename'] = $this->config->get('sms_config_gatename');
		}

		if (isset($this->request->post['sms_config_from'])) {
			$data['sms_config_from'] = $this->request->post['sms_config_from'];
		} else {
			$data['sms_config_from'] = $this->config->get('sms_config_from');
		}

		if (isset($this->request->post['sms_config_to'])) {
			$data['sms_config_to'] = $this->request->post['sms_config_to'];
		} else {
			$data['sms_config_to'] = $this->config->get('sms_config_to');
		}

		if (isset($this->request->post['sms_config_copy'])) {
			$data['sms_config_copy'] = $this->request->post['sms_config_copy'];
		} else {
			$data['sms_config_copy'] = $this->config->get('sms_config_copy');
		}
		
		if (isset($this->request->post['sms_config_message'])) {
			$data['sms_config_message'] = $this->request->post['sms_config_message'];
		} else {
			$data['sms_config_message'] = $this->config->get('sms_config_message');
		}

		if (isset($this->request->post['sms_config_gate_username'])) {
			$data['sms_config_gate_username'] = $this->request->post['sms_config_gate_username'];
		} else {
			$data['sms_config_gate_username'] = $this->config->get('sms_config_gate_username');
		}

		if (isset($this->request->post['sms_config_gate_password'])) {
			$data['sms_config_gate_password'] = $this->request->post['sms_config_gate_password'];
		} else {
			$data['sms_config_gate_password'] = $this->config->get('sms_config_gate_password');
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/sms_config.tpl', $data));
	}

	protected function validate(){
		if (!$this->user->hasPermission('modify', 'module/sms_config')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}