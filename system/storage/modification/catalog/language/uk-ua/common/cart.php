<?php

        // oct_techstore start
        $_['text_cart_heading'] = 'Кошик:';
        $_['text_cart_items']     = '<span class="cart-total-price">%s</span> <i class="fa fa-shopping-cart"><span class="count-quantity">%s</span></i>';
        // oct_techstore end
      
//Переклад: Том'як Олег з любов'ю до Української мови та легкості Opencart
// Text
$_['text_items']     = '%s товар(ів) - %s';
$_['text_empty']     = 'У кошику порожньо!';
$_['text_cart']      = 'Відкрити кошик';
$_['text_checkout']  = 'Оформити замовлення';
$_['text_recurring'] = 'Профіль платежу';