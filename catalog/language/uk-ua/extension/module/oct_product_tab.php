<?php
// Heading 
$_['heading_title'] = 'Наші товари';

$_['tab_latest'] = 'Нові надходження';
$_['tab_bestseller'] = 'ТОП продаж';//'Популярні';
$_['tab_featured'] = 'Рекомендовані';
$_['tab_special'] = 'Акції';
$_['tab_top_viewed'] = 'Найбільше популярні';

$_['text_isbn'] = 'ISBN: ';

$_['text_stock']        = 'Наявність: ';
$_['text_instock']      = 'В наявності';
// Text
$_['text_reviews']  = 'Засновано на %s відгуках.'; 
?>