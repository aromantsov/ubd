<?php
class ModelExtensionModuleCustomerImport extends Model {
	public function setCustomer($user){
		$query = $this->db->query("SELECT customer_id, custom_field FROM " . DB_PREFIX . "customer");
		foreach($query->rows as $customer){
			$char = json_decode($customer['custom_field'], true);

            if($char[1] == $user['code']){
            	$char[2] = $user['rate'];
            	$update_customer = json_encode($char, JSON_UNESCAPED_UNICODE);
            	"UPDATE " . DB_PREFIX . "customer SET custom_field = '" . $update_customer . "', code = '" . $user['code'] . "' WHERE customer_id = " . $customer['customer_id']
            	return;
            }
		}

		$fio = explode(' ', $user['fio']);

		if(count($fio) >= 4){
			$lastname_array = array($fio[0], $fio[1], $fio[2]);
			$lastname = implode(' ', $lastname_array);
			$name = $fio[3];
		}else{
            $lastname = $fio[0];

            if(isset($fio[1])){
                $name = $fio[1];
            }else{
            	$name = '';
            }    
		}

        $customer = array(
        	1 => $user['code'],
        	2 => $user['rate']
        );

        $custom_field = json_encode($customer, JSON_UNESCAPED_UNICODE);
       
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer (customer_group_id, firstname, lastname, custom_field, status, code) VALUES (1, \"" . htmlentities($name) . "\", \"" . htmlentities($lastname) . "\",'" . $custom_field . "', 1, '" . $user['code'] . "')");
	}
}	