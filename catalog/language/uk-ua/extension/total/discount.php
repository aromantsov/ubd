<?php
// Heading
$_['heading_title_incentive_program_accamulation'] = 'Накопичувальна знижка за загальну суму замовлень більше %s (%s)';
$_['heading_title_incentive_program_summ_current_order'] = 'Знижка за суму замовлення більше %s (%s)';
$_['personal_discount'] = 'Персональна знижка (%s)';
$_['code'] = 'по коду';
$_['admin_discount'] = 'Знижка від адміністратора (%s)';
$_['result_discount'] = 'З урахуванням максимально допустимої знижки на товар';

