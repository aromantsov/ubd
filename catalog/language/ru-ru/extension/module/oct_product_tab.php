<?php
// Heading 
$_['heading_title']  = 'Наши товары';

$_['tab_latest']     = 'Новые поступления';
$_['tab_bestseller'] = 'ТОП продаж';//'Популярные';
$_['tab_featured']   = 'Рекомендуемые';
$_['tab_special']    = 'Акции';
$_['tab_top_viewed'] = 'Самые просматриваемые';

$_['text_isbn'] = 'ISBN: ';

$_['text_stock']        = 'Наличие: ';
$_['text_instock']      = 'На складе';

// Text
$_['text_reviews']   = 'Основано на %s отзывах.';
?>