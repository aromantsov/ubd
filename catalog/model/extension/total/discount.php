<?php

class ModelExtensionTotalDiscount extends Model
{

    public function getTotal($total)
    {
        if ($this->config->get('discount_status'))
        {
            $this->load->language('extension/total/discount');
            $this->load->model('catalog/product');
            
            $discount_programm = $this->config->get('discount_incentive_program');
            $discount_value = $this->config->get('discount_value');
            $discount_to = $this->config->get('discount_to_summ');
            $discount_from = $this->config->get('discount_from_summ');
            $discount_type = $this->config->get('discount_type');
            $discount_customer_group = $this->config->get('discount_customer_group');

            if(isset($total['ad'])){
                $admin_discount = $total['ad'];
            }else{
                $admin_discount = 0;
            }

            $len = count($discount_value);

            for ($i = 0; $i < $len; $i++)
            {

                $guest = $discount_customer_group[$i] == 'guests' && $this->customer->getGroupId() == null;

                if ($discount_customer_group[$i] != 'all' && $discount_customer_group[$i] != $this->customer->getGroupId() && !$guest)
                    continue;


                if ($discount_programm[$i] == 'accumulation')
                {
                    $t = $this->getSummAllCompletedOrders();
                    $title_format = $this->language->get('heading_title_incentive_program_accamulation');
                } else if ($discount_programm[$i] == 'summ_current_order')
                {
                    $products = $this->cart->getProducts();
                    
                    $t=0;
                    $min_total = 0;
                    $all_delta_total = 0;
                    foreach ($products as $product)
                    {
                        $product_info = $this->model_catalog_product->getProduct($product['product_id']);

                        if($product_info['max_discount'] == 0){
                            $product_info['max_discount'] = 100;
                        }

                        $min_price = $product_info['price'] * (100 - $product_info['max_discount']) / 100;

                        if ((float)$product_info['special']) 
                            continue;
                        else
                            $t+=$product_info['price']*$product['quantity'];
                            $min_total += $min_price * $product['quantity'];
                            $delta_total = $t - $min_total;
                            $all_delta_total += $delta_total;
                    }

                    
                    $title_format = $this->language->get('heading_title_incentive_program_summ_current_order');
                } else
                    continue;

                if($t < $discount_from[0]){
                    if(isset($this->session->data['customer_id'])){
                        $query = $this->db->query("SELECT custom_field FROM " . DB_PREFIX . "customer WHERE customer_id = " . $this->session->data['customer_id']);

                        $custom_field = json_decode($query->row['custom_field'], true);

                        $rate = $custom_field[2];

                        $value = $total['total'] * $rate / 100;
                    }else{
                        $value = 0;
                    }

                    $ad_value = $total['total'] * $admin_discount / 100;

                    $title_format = $this->language->get('personal_discount');

                    $discount_from_currency = $rate . '%';

                    if($ad_value > $value){
                        $value = $ad_value;
                        $title_format = $this->language->get('admin_discount');
                        $discount_from_currency = $admin_discount . '%';
                    }

                    $total['totals'][] = array(
                        'code' => 'discount',
                        'title' => sprintf($title_format, $discount_from_currency),
                        'value' => $value,
                        'sort_order' => $this->config->get('discount_sort_order'),
                    );

                     if((isset($all_delta_total)) && ($value > $all_delta_total)){
                        $value = $all_delta_total;
                        $result_rate = round($value / $t *100, 0);
                        $title_format = $this->language->get('result_discount');
                        $discount_from_currency = $result_rate . '%';

                        $total['totals'][] = array(
                        'code' => 'discount',
                        'title' => sprintf($title_format, $discount_from_currency),
                        'value' => $value,
                        'sort_order' => $this->config->get('discount_sort_order'),
                        );
                    }
                    
                    $total['total'] -= $value;

                    break;
                }         

                if ($discount_from[$i] <= $t && $t < $discount_to[$i])
                {
                    if ($discount_type[$i] == 'precent')
                    {
                        $value = ($t / 100) * $discount_value[$i];

                        $discount_val = $discount_value[$i] . "%";
                    } else if ($discount_type[$i] == 'fixed_summ')
                    {
                        $value = $discount_value[$i];
                        $discount_val = $this->currency->format($discount_value[$i], $this->session->data['currency']);
                    } else
                        continue;

                    $discount_from_currency = $this->currency->format($discount_from[$i], $this->session->data['currency']);    

                    if(isset($this->session->data['customer_id'])){    

                        $query = $this->db->query("SELECT custom_field FROM " . DB_PREFIX . "customer WHERE customer_id = " . $this->session->data['customer_id']);

                        $custom_field = json_decode($query->row['custom_field'], true);

                        $rate = $custom_field[2];

                        $p_value = $total['total'] * $rate / 100;

                        if($p_value > $value){
                            $value = $p_value;
                            $title_format = $this->language->get('personal_discount');
                            $discount_from_currency = $rate . '%';
                        }
                    }

                    $ad_value = $total['total'] * $admin_discount / 100;

                    if($ad_value > $value){
                        $value = $ad_value;
                        $title_format = $this->language->get('admin_discount');
                        $discount_from_currency = $admin_discount . '%';
                    }

                    $total['totals'][] = array(
                        'code' => 'discount',
                        'title' => sprintf($title_format, $discount_from_currency, $discount_val),
                        'value' => $value,
                        'sort_order' => $this->config->get('discount_sort_order'),
                    );

                    if($value > $all_delta_total){
                        $value = $all_delta_total;
                        $title_format = $this->language->get('result_discount');

                        $total['totals'][] = array(
                        'code' => 'discount',
                        'title' => $title_format,
                        'value' => $value,
                        'sort_order' => $this->config->get('discount_sort_order'),
                        );
                    }
                    

                    $total['total'] -= $value;
                }
            }
        }
    }

    private function getSummAllCompletedOrders()
    {
        $this->load->model('account/order');
        $sum = 0;
        $complete_status_value = '5';

        $orders = $this->model_account_order->getOrders();
        foreach ($orders as $order)
        {
            $order_details = $this->model_account_order->getOrder($order['order_id']);
            if ($order_details['order_status_id'] == $complete_status_value)
                $sum+=$order_details['total'];
        }

        return $sum;
    }

}
