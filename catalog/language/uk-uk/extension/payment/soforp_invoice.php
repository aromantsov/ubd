<?php
// Text
$_['text_title']       = 'Безготівкова оплата (по перерахунку)';
$_['text_instruction'] = '<div class="text-center">Щоб роздрукувати рахунок - натисніть на кнопку нижче (відкриється в новому вікні)</div>';
$_['text_payment']     = '<div class="text-center">Замовлення не буде опрацьоване поки ми не отримаємо гроші</div>';
$_['text_order_history']  = 'Замовлення зберігається в <a href="{href}" target="_blank">Історії замовлень</a>';
$_['text_printpay']       = '<div class="text-center"><a style="display: inline-block !important;" href="{href}" class="btn btn-success" style="text-decoration:none;margin:0 auto;" target="_blank">Друк рахунку</a></div>';
$_['text_loading']       = 'Завантаження';
$_['text_month_1']  =   'Січня';
$_['text_month_2']  =   'Лютого';
$_['text_month_3']  =   'Березня';
$_['text_month_4']  =   'Квітня';
$_['text_month_5']  =   'Травня';
$_['text_month_6']  =   'Червня';
$_['text_month_7']  =   'Липня';
$_['text_month_8']  =   'Серпня';
$_['text_month_9']  =   'Вересня';
$_['text_month_10']  =   'Жовтня';
$_['text_month_11']  =   'Листопада';
$_['text_month_12']  =   'Грудня';
?>