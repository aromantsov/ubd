<?php
// Heading
$_['heading_title']      = 'Оплата безготівкою';

// Text
$_['text_payment']       = 'Оплата';
$_['text_success']       = 'Налаштування модуля обновлені!';
$_['text_edit']       = 'Редагувати';

// Entry
$_['supplier_info']      = 'Продавець / Постачальник:';
$_['entry_sheff']        = 'Керівник фірми:';
$_['entry_total']        = 'Нижня межа суми:';
$_['entry_checking_account'] = 'Р/р одержувача:';
$_['entry_bank']         = 'Банк отримувач:';
$_['entry_egrpou']       = 'Код отримувача (ЄДРПОУ / ІПН):';
$_['entry_mfo']          = 'МФО банку отримувача:';
$_['entry_order_status'] = 'Статус замовлення після оплати:';
$_['entry_geo_zone']     = 'Географічна зона:';
$_['entry_status']       = 'Статус:';
$_['entry_tax_status']   = 'Враховувати податки:';
$_['entry_sort_order']   = 'Порядок сортування:';

// Error
$_['error_permission']   = 'У Вас нема прав для керування цим модулем!';
$_['error_supplier_info']= 'Це поле обовязкове для заповнення!';
?>