<?php
/**************************************************************/
/*	@copyright	OCTemplates 2018.							  */
/*	@support	https://octemplates.net/					  */
/*	@license	LICENSE.txt									  */
/**************************************************************/

class ControllerExtensionModuleOctPopupOpt extends Controller {
    public function index() {
        $data = array();
        
        $this->load->model('catalog/product');
        $this->load->language('extension/module/oct_popup_opt');
        
        $oct_popup_opt_data         = (array) $this->config->get('oct_popup_opt_data');
        $data['oct_popup_opt_data'] = $oct_popup_opt_data;
        
        // oct_advanced_options_settings start
        $data['oct_advanced_options_settings_data'] = $this->config->get('oct_advanced_options_settings_data');
        // oct_advanced_options_settings end
        
        if (isset($this->request->get['product_id'])) {
            $product_id = (int) $this->request->get['product_id'];
        } else {
            $product_id = 0;
        }
        
        $data['mask'] = ($oct_popup_opt_data['mask']) ? $oct_popup_opt_data['mask'] : '';
        
        $product_info = $this->model_catalog_product->getProduct($product_id);
        
        $data['product_id'] = $product_id;
        
        if ($product_info) {
            $data['heading_title']   = $this->language->get('heading_title');
            $data['button_shopping'] = $this->language->get('button_shopping');
            $data['button_checkout'] = $this->language->get('button_checkout');
            $data['button_upload']   = $this->language->get('button_upload');
            $data['text_price']      = $this->language->get('text_price');
            $data['text_reward']     = $this->language->get('text_reward');
            $data['text_points']     = $this->language->get('text_points');
            $data['text_stock']      = $this->language->get('text_stock');
            $data['text_discount']   = $this->language->get('text_discount');
            $data['text_tax']        = $this->language->get('text_tax');
            $data['enter_firstname'] = $this->language->get('enter_name');
            $data['enter_telephone'] = $this->language->get('enter_telephone');
            $data['enter_email']     = $this->language->get('enter_email');
            $data['enter_comment']   = $this->language->get('enter_comment');
            $data['entry_quantity']  = $this->language->get('entry_quantity');
            $data['text_minimum']    = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
            $data['text_select']     = $this->language->get('text_select');
            $data['text_option']     = $this->language->get('text_option');
            $data['text_loading']    = $this->language->get('text_loading');
            $data['text_opt']        = $this->language->get('text_opt');
            
            if (!$oct_popup_opt_data['stock_check'] && $product_info['quantity'] <= 0) {
                $data['stock_warning'] = $product_info['stock_status'];
            } else {
                $data['stock_warning'] = '';
            }
            
            $data['product_name'] = $product_info['name'];
            
            $this->load->model('tool/image');
            
            $image_width  = ($oct_popup_opt_data['image_width']) ? $oct_popup_opt_data['image_width'] : '152';
            $image_height = ($oct_popup_opt_data['image_height']) ? $oct_popup_opt_data['image_height'] : '152';
            
            if ($product_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($product_info['image'], $image_width, $image_height);
            } else {
                $data['thumb'] = $this->model_tool_image->resize("placeholder.png", $image_width, $image_height);
            }
            
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $data['price'] = false;
            }
            
            if ((float) $product_info['special']) {
                $data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $data['special'] = false;
            }
            
            if ((float) $product_info['special']) {
                $data['economy'] = round((($product_info['price'] - $product_info['special']) / ($product_info['price'] + 0.01)) * 100, 0);
            } else {
                $data['economy'] = false;
            }
            
            if ($this->config->get('config_tax')) {
                $data['tax'] = $this->currency->format((float) $product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
            } else {
                $data['tax'] = false;
            }
            
            $data['points'] = $product_info['points'];
            
            $discounts = $this->model_catalog_product->getProductDiscounts($product_id);
            
            $data['discounts'] = array();
            
            foreach ($discounts as $discount) {
                $data['discounts'][] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
                );
            }
            
            if ($oct_popup_opt_data['description_max']) {
                $data['description'] = utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $oct_popup_opt_data['description_max']) . '...';
            } else {
                $data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
            }
            
            $data['firstname'] = ($this->customer->isLogged()) ? $this->customer->getFirstName() : '';
            $data['email']     = ($this->customer->isLogged()) ? $this->customer->getEmail() : '';
            $data['telephone'] = ($this->customer->isLogged()) ? $this->customer->getTelephone() : '';
            $data['comment']   = '';
            
            if ($product_info['minimum']) {
                $data['minimum'] = $product_info['minimum'];
            } else {
                $data['minimum'] = 1;
            }
            
            $oct_data = $this->config->get('oct_techstore_data');
            
            if (isset($oct_data['terms']) && $oct_data['terms']) {
                $this->load->model('catalog/information');
                
                $information_info = $this->model_catalog_information->getInformation($oct_data['terms']);
                
                if ($information_info) {
                    $data['text_terms'] = sprintf($this->language->get('text_oct_terms'), $this->url->link('information/information', 'information_id=' . $oct_data['terms'], 'SSL'), $information_info['title'], $information_info['title']);
                } else {
                    $data['text_terms'] = '';
                }
            } else {
                $data['text_terms'] = '';
            }
            
            $data['options'] = array();
            
            foreach ($this->model_catalog_product->getProductOptions($product_id) as $option) {
                $product_option_value_data = array();
                
                if (isset($oct_popup_opt_data['allowed_options']) && (in_array($option['option_id'], $oct_popup_opt_data['allowed_options']))) {
                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float) $option_value['price']) {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $price = false;
                            }
                            
                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $option_value['image'] ? $this->model_tool_image->resize($option_value['image'], 50, 50) : '',
                                'price' => $price,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }
                    
                    $data['options'][] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required']
                    );
                }
            }
            
            $data['recurrings'] = $this->model_catalog_product->getProfiles($product_id);
            
            $this->response->setOutput($this->load->view('extension/module/oct_popup_opt', $data));
        } else {
            $this->response->redirect($this->url->link('checkout/cart'));
        }
    }
    
    public function send() {

        $json = array();
        
        $this->language->load('extension/module/oct_popup_opt');
        
        $this->load->model('extension/module/oct_popup_call_phone');
        
        $oct_popup_call_phone_data = (array) $this->config->get('oct_popup_call_phone_data');
        
        if (isset($this->request->post['name'])) {
            if ((isset($oct_popup_call_phone_data['name']) && $oct_popup_call_phone_data['name'] == 2) && (utf8_strlen(trim($this->request->post['name'])) < 1) || (utf8_strlen(trim($this->request->post['name'])) > 32)) {
                $json['error']['field']['name'] = $this->language->get('error_name');
            }
        } else {
            if ((isset($oct_popup_call_phone_data['name']) && $oct_popup_call_phone_data['name'] == 2) && (utf8_strlen(trim($this->request->post['name'])) < 1) || (utf8_strlen(trim($this->request->post['name'])) > 32)) {
                $json['error']['field']['name'] = $this->language->get('error_name');
            }
        }
        
        // if (isset($this->request->post['time'])) {
        //     if (isset($oct_popup_call_phone_data['time']) && $oct_popup_call_phone_data['time'] == 2) {
        //         if (empty($this->request->post['time'])) {
        //             $json['error']['field']['time'] = $this->language->get('error_time');
        //         }
        //     }
        // } else {
        //     if (isset($oct_popup_call_phone_data['time']) && $oct_popup_call_phone_data['time'] == 2) {
        //         if (empty($this->request->post['time'])) {
        //             $json['error']['field']['time'] = $this->language->get('error_time');
        //         }
        //     }
        // }
        
        if (isset($this->request->post['telephone'])) {
            if ((isset($oct_popup_call_phone_data['telephone']) && $oct_popup_call_phone_data['telephone'] == 2) && (utf8_strlen(str_replace(array(
                '_',
                '-',
                '(',
                ')',
                '+'
            ), "", $this->request->post['telephone'])) < 3 || utf8_strlen(str_replace(array(
                '_',
                '-',
                '(',
                ')',
                '+'
            ), "", $this->request->post['telephone'])) > 32)) {
                $json['error']['field']['telephone'] = $this->language->get('error_telephone');
            }
        } else {
            if ((isset($oct_popup_call_phone_data['telephone']) && $oct_popup_call_phone_data['telephone'] == 2) && (utf8_strlen(str_replace(array(
                '_',
                '-',
                '(',
                ')',
                '+'
            ), "", $this->request->post['telephone'])) < 3 || utf8_strlen(str_replace(array(
                '_',
                '-',
                '(',
                ')',
                '+'
            ), "", $this->request->post['telephone'])) > 32)) {
                $json['error']['field']['telephone'] = $this->language->get('error_telephone');
            }
        }
        
        if (isset($this->request->post['comment'])) {
            if ((isset($oct_popup_call_phone_data['comment']) && $oct_popup_call_phone_data['comment'] == 2) && (utf8_strlen($this->request->post['comment']) < 3) || (utf8_strlen($this->request->post['comment']) > 500)) {
                $json['error']['field']['comment'] = $this->language->get('error_comment');
            }
        } else {
            if (isset($oct_popup_call_phone_data['comment']) && $oct_popup_call_phone_data['comment'] == 2) {
                $json['error']['field']['comment'] = $this->language->get('error_comment');
            }
        }
        
        $oct_data = $this->config->get('oct_techstore_data');
        
        if (isset($oct_data['terms']) && $oct_data['terms']) {
            if (!isset($this->request->post['terms'])) {
                $this->load->model('catalog/information');
                
                $information_info = $this->model_catalog_information->getInformation($oct_data['terms']);
                
                $json['error']['field']['terms'] = sprintf($this->language->get('error_oct_terms'), $information_info['title']);
            }
        }
        
        $data = array();
        
        if (!isset($json['error'])) {
            
            $post_data = $this->request->post;

            if (isset($post_data['product_id'])) {
                $this->load->model('catalog/product');
                $product_info = $this->model_catalog_product->getProduct($post_data['product_id']);
                // $data[] = array(
                //     'name' => $this->language->get('enter_name'),
                //     'value' => $post_data['name']
                // );
                $data[] = array(
                    'name' => 'product_id',
                    'value' => $post_data['product_id']
                );
                $data[] = array(
                    'name' => $this->language->get('product_name'),
                    'value' => $product_info['name']
                );
                $data[] = array(
                    'name' => 'isbn',
                    'value' => $product_info['isbn']
                );
                $data[] = array(
                    'name' => $this->language->get('product_price'),
                    'value' => $product_info['price']
                );
            }
            
            if (isset($post_data['name'])) {
                $data[] = array(
                    'name' => $this->language->get('enter_name'),
                    'value' => $post_data['name']
                );
            }
            
            if (isset($post_data['telephone'])) {
                $data[] = array(
                    'name' => $this->language->get('enter_telephone'),
                    'value' => $post_data['telephone']
                );
            }
            
            if (isset($post_data['comment'])) {
                $data[] = array(
                    'name' => $this->language->get('enter_comment'),
                    'value' => $post_data['comment']
                );
            }

            if (isset($post_data['email'])) {
                $data[] = array(
                    'name' => 'Email',
                    'value' => $post_data['email']
                );
            }
            
            if (isset($post_data['time'])) {
                $data[] = array(
                    'name' => $this->language->get('enter_time'),
                    'value' => $post_data['time']
                );
            }
            
            $data_send = array(
                'info' => serialize($data)
            );
            
            $this->model_extension_module_oct_popup_call_phone->addRequest($data_send);
            
            $json['output'] = $this->language->get('text_success_send');
            
            if ($oct_popup_call_phone_data['notify_status']) {
                $html_data['date_added']      = date('d.m.Y H:i:s', time());
                $html_data['logo']            = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
                $html_data['store_name']      = $this->config->get('config_name');
                $html_data['store_url']       = $this->config->get('config_url');
                $html_data['text_info']       = $this->language->get('text_info');
                $html_data['text_date_added'] = $this->language->get('text_date_added');
                $html_data['data_info']       = $data;
                
                $html = $this->load->view('mail/oct_popup_call_phone_mail', $html_data);
                
                if (version_compare(VERSION, '2.0.2', '<')) {
                    $mail = new Mail($this->config->get('config_mail'));
                } else {
                    $mail                = new Mail();
                    $mail->protocol      = $this->config->get('config_mail_protocol');
                    $mail->parameter     = $this->config->get('config_mail_parameter');
                    $mail->smtp_hostname = (version_compare(VERSION, '2.0.3', '<')) ? $this->config->get('config_mail_smtp_host') : $this->config->get('config_mail_smtp_hostname');
                    $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                    $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                    $mail->smtp_port     = $this->config->get('config_mail_smtp_port');
                    $mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout');
                }
                
                $mail->setFrom($this->config->get('config_email'));
                $mail->setSender($this->config->get('config_name'));
                $mail->setSubject($this->language->get('heading_title') . " -- " . $html_data['date_added']);
                $mail->setHtml($html);
                
                $emails = explode(',', $oct_popup_call_phone_data['notify_email']);
                
                foreach ($emails as $email) {
                    if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
                        $mail->setTo($email);
                        $mail->send();
                    }
                }
            }
        }
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
    }
    
    public function update_prices() {
        $json = array();
        
        if (isset($this->request->request['product_id']) && isset($this->request->request['quantity'])) {
            $this->load->model('catalog/product');
            
            $option_price    = 0;
            $product_id      = (int) $this->request->request['product_id'];
            $quantity        = (int) $this->request->request['quantity'];
            $product_info    = $this->model_catalog_product->getProduct($product_id);
            $product_options = $this->model_catalog_product->getProductOptions($product_id);
            
            if (!empty($this->request->request['option'])) {
                $options = $this->request->request['option'];
            } else {
                $options = array();
            }
            
            $options_arr = array();
            
            if ($options) {
                foreach ($options as $key => $option) {
                    if (is_array($option)) {
                        foreach ($option as $option_value) {
                            $product_option_value_id_data = explode("|", $option_value);
                            if (isset($product_option_value_id_data[0]) && isset($product_option_value_id_data[1])) {
                                $options_arr[$key][$product_option_value_id_data[0]] = array(
                                    'product_option_value_id' => $product_option_value_id_data[0],
                                    'quantity' => $product_option_value_id_data[1]
                                );
                            }
                        }
                    }
                }
            }
            
            foreach ($product_options as $product_option) {
                if (is_array($product_option['product_option_value'])) {
                    foreach ($product_option['product_option_value'] as $option_value) {
                        if ($product_option['type'] == 'oct_quantity') {
                            if (isset($options_arr[$product_option['product_option_id']][$option_value['product_option_value_id']])) {
                                if (($options_arr[$product_option['product_option_id']][$option_value['product_option_value_id']]['product_option_value_id'] == $option_value['product_option_value_id'])) {
                                    $oct_quantity = (isset($options_arr[$product_option['product_option_id']][$option_value['product_option_value_id']]['quantity'])) ? $options_arr[$product_option['product_option_id']][$option_value['product_option_value_id']]['quantity'] : 1;
                                    if ($option_value['price_prefix'] == '+') {
                                        $option_price += $option_value['price'] * $oct_quantity;
                                    } elseif ($option_value['price_prefix'] == '-') {
                                        $option_price -= $option_value['price'] * $oct_quantity;
                                    }
                                }
                            }
                        } else {
                            if (isset($options[$product_option['product_option_id']])) {
                                if (($options[$product_option['product_option_id']] == $option_value['product_option_value_id']) || ((is_array($options[$product_option['product_option_id']])) && (in_array($option_value['product_option_value_id'], $options[$product_option['product_option_id']])))) {
                                    if ($option_value['price_prefix'] == '+') {
                                        $option_price += $option_value['price'];
                                    } elseif ($option_value['price_prefix'] == '-') {
                                        $option_price -= $option_value['price'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            $json = array();
            
            $json['special'] = $this->currency->format(($this->tax->calculate($this->get_price_discount($product_id, $quantity, 'special'), $product_info['tax_class_id'], $this->config->get('config_tax')) * $quantity) + $this->tax->calculate($option_price * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            
            if ($json['special']) {
                $economy          = round(((($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')) * $quantity) - ($this->tax->calculate($this->get_price_discount($product_id, $quantity, 'special'), $product_info['tax_class_id'], $this->config->get('config_tax')) * $quantity)) / (($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')) * $quantity) + 0.01)) * 100, 0);
                $saver            = round(($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))) - ($this->tax->calculate($this->get_price_discount($product_id, $quantity, 'special'), $product_info['tax_class_id'], $this->config->get('config_tax'))));
                //$json['you_save'] = $this->currency->format(($this->tax->calculate($saver, $product_info['tax_class_id'], $this->config->get('config_tax')) * $quantity) + $this->tax->calculate($quantity * $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) . ' (-'.$economy.'%)';
                $json['you_save'] = '-' . $economy . '%';
            } else {
                $json['you_save'] = false;
            }
            
            $json['price'] = $this->currency->format(($this->tax->calculate($this->get_price_discount($product_id, $quantity, 'price'), $product_info['tax_class_id'], $this->config->get('config_tax')) * $quantity) + $this->tax->calculate($option_price * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            
            $json['tax'] = $this->currency->format(($this->get_price_discount($product_id, $quantity, 'price') + $option_price) * $quantity, $this->session->data['currency']);
        }
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
    
    public function get_price_discount($product_id, $quantity, $type) {
        $this->load->model('catalog/product');
        
        $customer_group_id = ($this->customer->isLogged()) ? (int) $this->customer->getGroupId() : (int) $this->config->get('config_customer_group_id');
        
        $product_info = (array) $this->model_catalog_product->getProduct($product_id);
        
        if ($type == 'price') {
            $product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "' AND customer_group_id = '" . (int) $customer_group_id . "' AND quantity <= '" . (int) $quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");
            if ($product_discount_query->row) {
                $price = $product_discount_query->row['price'];
            } else {
                $price = $product_info['price'];
            }
        }
        
        if ($type == 'special') {
            $product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "' AND customer_group_id = '" . (int) $customer_group_id . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");
            if ($product_special_query->row) {
                $price = $product_special_query->row['price'];
            } else {
                $price = $product_info['price'];
            }
        }
        
        return $price;
    }
}