<?php
class ControllerExtensionModuleCustomerImport extends Controller {
	public function index(){

		set_time_limit(0);

        $this->load->language('extension/module/customer_import');

	    // if(empty($this->request->post['link'])){
     //        $this->session->data['error'] = $this->language->get('no_link');
     //        $this->index();
     //        return;
     //    }

		$this->load->model('extension/module/customer_import');
		
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'http://api.ubd.ua/dc.json');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "task=dc");
        $json = curl_exec($curl);

		if($json){

		    $customers = json_decode($json, true);

		    if(is_array($customers)){

		        $customers = $customers['result'];

		        foreach($customers as $customer){
                    $this->model_extension_module_customer_import->setCustomer($customer);
		        }

                $this->session->data['success'] = $this->language->get('text_success');

		        $this->index();

		    }else{
			    $this->session->data['error'] = $this->language->get('no_json');
			    $this->index();
		    }
		}
	}
}