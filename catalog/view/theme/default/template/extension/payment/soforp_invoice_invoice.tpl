<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ru" xml:lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Рахунок на оплату</title>
<link rel="stylesheet" type="text/css" href="<?php echo $style; ?>" />
</head>
<body>
<div style="width: 700px">
    <table>
        <tbody>
            <tr>
                <th align="right">Постачальник:</th>
                <td><?php echo $supplier_info; ?></td>
            </tr>
            <tr>
                <th align="right">Одержувач:</th>
                <td><?php echo $name; ?><br />тел. <?php echo $telephone; ?></td>
            </tr>
            <tr>
                <th align="right">Платник:</th>
                <td><?php echo $payer; ?></td>
            </tr>
            <tr>
                <th align="right">Валюта:</th>
                <td>Гривня</td>
            </tr>
            <tr>
                <th align="right">Р/р одержувача:</th>
                <td><?php echo $checking_account; ?></td>
            </tr>
            <tr>
                <th align="right">Банк отримувач:</th>
                <td><?php echo $bank; ?></td>
            </tr>
            <tr>
                <th align="right">Код отримувача (ЄДРПОУ / ІПН):</th>
                <td><?php echo $egrpou; ?></td>
            </tr>
            <tr>
                <th align="right">МФО банку отримувача:</th>
                <td><?php echo $mfo; ?></td>
            </tr>
        </tbody>
    </table>
    <br />
    <br />
    <center>
        <h3>Рахунок на оплату №<?php echo $order_id; ?><br />від <?php echo $date_added; ?> р.</h3>
    </center>
    <br />
    <br />
    <table class="product">
        <tbody>
            <tr class="heading">
                <td>№</td>
                <td style="width:50%">Товар</td>
                <td>Од. виміру</td>
                <td>Кількість (об'єм, обсяг)</td>
                <td>Цiна без ПДВ</td>
                <td>Сума без ПДВ</td>
            </tr>
            <?php $i=0;foreach($products as $row): ?>
            <tr>
                <td><?php echo ++$i; ?></td>
                <td style="text-align:left"><?php echo $row['name']; ?></td>
                <td>шт.</td>
                <td><?php echo $row['quantity']; ?></td>
                <td><?php printf('%01.2f', round($row['price'], 2)); ?></td>
                <td><?php printf('%01.2f', round($row['total'], 2)); ?></td>
            </tr>
            <?php endforeach; ?>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td colspan="5" style="text-align:right;border-left:1px solid #fff;border-bottom:1px solid #fff;font-weight:bold;">
                <?php if ($total['title']=='Предварительная стоимость' || $total['title']=='Сумма') { 
                    echo 'Разом без ПДВ:';
                } elseif ($total['title']=='Итого') {
                    echo 'Всього:';
                } else {
                    echo $total['title'];
                } ?>
                </td>
              <td class="text-right"><?php printf('%01.2f', round($total['text'], 2)); ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <p>Всього на суму: <strong><?php echo $sum_str;?></strong></p>
<!--     <div style="height:233px;margin-top:-60px">
    <p style="text-align:right;padding-top:100px">Виписав(ла): <?php echo $manager; ?></p>
    <p style="text-align:right;margin-top:-10px"><?php echo $sheff; ?></p>
    <p style="text-align:right;font-weight:bold">Рахунок дійсний до сплати до <?php echo $deadline; ?></p>
    </div> -->
</div>

</body>
</html>