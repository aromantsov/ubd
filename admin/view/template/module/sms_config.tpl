<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-sms-config" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
        <h1><?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="container-fluid">
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
        </div>
        <div class="panel-body">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sms-config" class="form-horizontal">
            <div class="form-group">
              <label class="col-sm-2 control-label" for=""><?php echo $entry_sms_alert; ?></label>
              <div class="col-sm-10">
                <select name="sms_config_alert" id="input-sms-config-alert" class="form-control">
                  <?php if ($sms_config_alert) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"
              for="input-sms-config-gatename"><?php echo $entry_sms_gatename; ?></label>
              <div class="col-sm-10">
                <select name="sms_config_gatename" id="input-sms-config-gatename" class="form-control">
                  <?php foreach ($sms_gatenames as $sms_gatename) { ?>
                  <?php if ($sms_config_gatename == $sms_gatename) { ?>
                  <option value="<?php echo $sms_gatename; ?>"
                  selected="selected"><?php echo $sms_gatename; ?></option>
                  <?php } else { ?>
                  <option
                  value="<?php echo $sms_gatename; ?>"><?php echo $sms_gatename; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-sms-config-from"><span
                data-toggle="tooltip"
              title="<?php echo $help_sms_from; ?>"><?php echo $entry_sms_from; ?></span></label>
              <div class="col-sm-10">
                <input type="text" name="sms_config_from"
                value="<?php echo $sms_config_from; ?>"
                placeholder="<?php echo $entry_sms_from; ?>" id="input-sms-config-from"
                class="form-control"/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-sms-config-to"><span
                data-toggle="tooltip"
              title="<?php echo $help_sms_to; ?>"><?php echo $entry_sms_to; ?></span></label>
              <div class="col-sm-10">
                <input type="text" name="sms_config_to" value="<?php echo $sms_config_to; ?>"
                placeholder="<?php echo $entry_sms_to; ?>" id="input-sms-config-to"
                class="form-control"/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-sms-config-copy"><span
                data-toggle="tooltip"
              title="<?php echo $help_sms_copy; ?>"><?php echo $entry_sms_copy; ?></span></label>
              <div class="col-sm-10">
                <textarea name="sms_config_copy" rows="5"
                placeholder="<?php echo $entry_sms_copy; ?>"
                id="input-sms-config-copy"
                class="form-control"><?php echo $sms_config_copy; ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-sms-config-message"><?php echo $entry_sms_message; ?></label>
              <div class="col-sm-3">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <?php echo $help_sms_message; ?>
                  </div>
                </div>
              </div>
              <div class="col-sm-7">
                <textarea name="sms_config_message" rows="5"
                placeholder="<?php echo $entry_sms_message; ?>"
                id="input-sms-config-message"
                class="form-control"><?php echo $sms_config_message; ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"
              for="input-sms-gate-username"><?php echo $entry_sms_gate_username; ?></label>
              <div class="col-sm-10">
                <input type="text" name="sms_config_gate_username"
                value="<?php echo $sms_config_gate_username; ?>"
                placeholder="<?php echo $entry_sms_gate_username; ?>" id="input-sms-gate-username"
                class="form-control"/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"
              for="input-sms-config-gate-password"><?php echo $entry_sms_gate_password; ?></label>
              <div class="col-sm-10">
                <input type="password" name="sms_config_gate_password"
                value="<?php echo $sms_config_gate_password; ?>"
                placeholder="<?php echo $entry_sms_gate_password; ?>"
                id="input-sms-config-gate-password" class="form-control"/>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div></div>
    <?php echo $footer; ?>