<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-cheque" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-cheque" class="form-horizontal">

        <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
                  <select name="soforp_invoice_status" id="soforp_invoice_status" class="form-control">
                          <?php if ($soforp_invoice_status) { ?>
                          <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                          <option value="0"><?php echo $text_disabled; ?></option>
                          <?php } else { ?>
                          <option value="1"><?php echo $text_enabled; ?></option>
                          <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                          <?php } ?>
                      </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_tax_status"><?php echo $entry_tax_status; ?></label>
            <div class="col-sm-10">
                  <select name="soforp_invoice_tax_status" id="soforp_invoice_tax_status" class="form-control">
                          <?php if ($soforp_invoice_tax_status) { ?>
                          <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                          <option value="0"><?php echo $text_disabled; ?></option>
                          <?php } else { ?>
                          <option value="1"><?php echo $text_enabled; ?></option>
                          <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                          <?php } ?>
                      </select>
            </div>
          </div>
 <div class="form-group required">
          <label class="col-sm-2 control-label" for="soforp_invoice_supplier_info_<?php echo $language['language_id']; ?>"><?php echo $supplier_info; ?></label>
          <?php foreach ($languages as $language) { ?>
         
            
            <div class="col-sm-10 pull-right">
              <div class="input-group" style="margin-bottom:20px;">
              <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                <textarea class="form-control"  name="soforp_invoice_supplier_info_<?php echo $language['language_id']; ?>" id=="soforp_invoice_supplier_info_<?php echo $language['language_id']; ?>" rows="3"><?php echo isset(${'soforp_invoice_supplier_info_' . $language['language_id']}) ? ${'soforp_invoice_supplier_info_' . $language['language_id']} : ''; ?></textarea><br />
                              <?php if (isset(${'error_supplier_info_' . $language['language_id']}))
                              { ?>
                                  <span class="error"><?php echo ${'error_supplier_info_' . $language['language_id']}; ?></span>
    <?php } ?>
            </div>
          </div>
          
          <?php } ?>
  </div>
  
          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_sheff"><?php echo $entry_sheff; ?></label>
            <div class="col-sm-10">
                  <input type="text" name="soforp_invoice_sheff" id="soforp_invoice_sheff" value="<?php echo $soforp_invoice_sheff; ?>" class="form-control"/>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_total"><?php echo $entry_total; ?></label>
            <div class="col-sm-10">
                  <input type="text" name="soforp_invoice_total" id="soforp_invoice_total" value="<?php echo $soforp_invoice_total; ?>" class="form-control"/>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_checking_account"><?php echo $entry_checking_account; ?></label>
            <div class="col-sm-10">
                  <input type="text" name="soforp_invoice_checking_account" id="soforp_invoice_checking_account" value="<?php echo $soforp_invoice_checking_account; ?>" class="form-control"/>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_bank"><?php echo $entry_bank; ?></label>
            <div class="col-sm-10">
                  <input type="text" name="soforp_invoice_bank" id="soforp_invoice_bank" value="<?php echo $soforp_invoice_bank; ?>" class="form-control"/>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_egrpou"><?php echo $entry_egrpou; ?></label>
            <div class="col-sm-10">
                  <input type="text" name="soforp_invoice_egrpou" id="soforp_invoice_egrpou" value="<?php echo $soforp_invoice_egrpou; ?>" class="form-control"/>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_mfo"><?php echo $entry_mfo; ?></label>
            <div class="col-sm-10">
                  <input type="text" name="soforp_invoice_mfo" id="soforp_invoice_mfo" value="<?php echo $soforp_invoice_mfo; ?>" class="form-control"/>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_order_status_id"><?php echo $entry_order_status; ?></label>
            <div class="col-sm-10">
                  <select name="soforp_invoice_order_status_id" id="soforp_invoice_order_status_id" class="form-control">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $soforp_invoice_order_status_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_geo_zone_id"><?php echo $entry_geo_zone; ?></label>
            <div class="col-sm-10">
                 <select name="soforp_invoice_geo_zone_id" id="soforp_invoice_geo_zone_id" class="form-control">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $soforp_invoice_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="soforp_invoice_sort_order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
                 <input type="text" name="soforp_invoice_sort_order" id="soforp_invoice_sort_order" value="<?php echo $soforp_invoice_sort_order; ?>"  class="form-control" />
              </select>
            </div>
          </div>
      </form>
    </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>