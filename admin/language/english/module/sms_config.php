<?php
// Heading
$_['heading_title']    			= 'Sms config';

// Text
$_['text_module']     			= 'Modules';
$_['text_success']     			= 'Success: You have modified module!';
$_['text_edit']        			= 'Edit Sms Config Module';

// Entry
$_['entry_sms_gatename']        = 'SMS gate:';
$_['entry_sms_to']              = 'Phone number to:';
$_['entry_sms_from']            = 'Phone number from:';
$_['entry_sms_message']         = 'Message text';
$_['entry_sms_gate_username']   = 'Sms gate Login';
$_['entry_sms_gate_password']   = 'Sms gate Password';
$_['entry_sms_alert']           = 'Enable SMS notification';
$_['entry_sms_copy']            = 'Additional phone numbers';

$_['help_sms_to']               = 'In the international format, only digits 1800xxxxxxx';
$_['help_sms_from']             = 'No more than 11 characters or the phone number up to 15 digits';
$_['help_sms_copy']             = 'Separated by commas, in the international format, without separators 1800xxxxxxx';
$_['help_sms_message']          = 'Availaible tags:<br/>{ID} - Order ID<br/>{DATE} - Order date<br/>{TIME} - Order time<br/>{SUM} - Order total<br/>{PHONE} - Customer phone';

// Error
$_['error_permission'] 			= 'Warning: You do not have permission to modify sms config module!';