<?php
class ModelExtensionPaymentSoforpInvoice extends Model {
	public function editSoforp($data){
		$sql = $this->db->query("SELECT name FROM " . DB_PREFIX . "soforp_invoice");
        $keys = array();

        for($i = 0; $i < $sql->num_rows; $i++){
            $keys[$i] = $sql->rows[$i]['name'];
        }

        foreach($data as $key => $value){
        	$db_key = str_replace('soforp_invoice_', '', $key);

        	if(in_array($db_key, $keys)){
                $this->db->query("UPDATE " . DB_PREFIX . "soforp_invoice SET value = '" . $value . "' WHERE name = '" . $db_key . "'");
        	}
        }
	}

	public function getSoforp(){
		$sql = $this->db->query("SELECT name, value FROM " . DB_PREFIX . "soforp_invoice");

		$soforp = array();

		foreach($sql->rows as $data){
			$soforp[$data['name']] = $data['value'];
		}

		return $soforp;
	}
}