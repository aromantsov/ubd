<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<body>
<div class="container">
  <?php foreach ($orders as $order) { ?>
  <div style="page-break-after: auto;">
    <h3><?php echo $text_picklist; ?> #<?php echo $order['order_id']; ?></h3>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo "ISBN"; ?></b></td>
          <td><b><?php echo $column_product; ?></b></td>
          <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
          <td>
            <?php if ($product['isbn']) { ?>
			<?php echo $product['isbn']; ?>
            <?php } ?></td>
          <td><?php echo $product['name']; ?></td>
		  <?php if ($product['quantity'] != '1') { ?>
	          <td class="text-right" bgcolor="yellow">
		  <?php } else {?>
	          <td class="text-right">
		  <?php } ?>
		  <b><?php echo $product['quantity']; ?></b></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php } ?>
  </div>
</div>
</body>
</html>