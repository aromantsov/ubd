<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p>
  </div>
</footer>

<?php if($route == 'checkout/checkout'){ ?>
<script type="text/javascript"><!--
    // Autocomplete for shipping addresses
    (function ($) {
        var methods = {
            init: function (options) {
                return this.each(function () {
                    var $this = $(this);
                    var data = $this.data('autocompleteAddress');

                    // If the plugin is not yet initialized
                    if (!data) {
                        $this.timer = null;
                        $this.items = new Array();

                        $.extend($this, options);

                        $this.attr('autocomplete', 'off');

                        // Focus
                        $this.on('focus.autocompleteAddress', function () {
                            $this.request('');
                        } );

                        // Blur
                        $this.on('blur.autocompleteAddress', function () {
                            setTimeout(function (object) {
                                object.hide();
                            }, 200, $this);
                        } );

                        // Keydown
                        $this.on('keydown.autocompleteAddress', function (event) {
                            switch (event.keyCode) {
                                case 27: // escape
                                    $this.hide();
                                    break;
                                default:
                                    $this.request();
                                    break;
                            }
                        } );

                        // Click
                        $this.click = function (event) {
                            event.preventDefault();

                            var value = $(event.target).parent().attr('data-value');

                            if (value && $this.items[value]) {
                                $this.select($this.items[value]);
                            }
                        }

                        // Show
                        $this.show = function () {
                            var pos = $this.position();

                            $this.siblings('ul.' + $this.class).css({
                                'top': pos.top + $this.outerHeight(),
                                'left': pos.left
                            });

                            $this.siblings('ul.' + $this.class).show();
                        }

                        // Hide
                        $this.hide = function () {
                            $this.siblings('ul.' + $this.class).hide();
                        }

                        // Request
                        $this.request = function (search) {
                            clearTimeout($this.timer);

                            $this.timer = setTimeout(function (object) {
                                search = (typeof(search) === 'undefined') ? object.val() : search;

                                object.source(search, $.proxy(object.response, object));
                            }, 200, $this);
                        }

                        // Response
                        $this.response = function (json) {
                            var html = '';

                            if (json.length) {
                                for (i = 0; i < json.length; i++) {
                                    this.items[json[i]['value']] = json[i];

                                    html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                                }
                            }

                            if (html && $this.is(':focus')) {
                                $this.show();
                            } else {
                                $this.hide();
                            }

                            $this.siblings('ul.' + $this.class).html(html);
                        }

                        $this.after('<ul class="' + $this.class + '"></ul>');
                        $this.siblings('ul.' + $this.class).delegate('a', 'click', $.proxy($this.click, $this));
                        $this.data('autocompleteAddress', true);
                    }
                } );
            },
            destroy: function () {
                return this.each(function () {
                    var $this = $(this);

                    $this.removeData('autocompleteAddress');

                    $this.off('.autocompleteAddress');
                } );
            }
        };

        $.fn.autocompleteAddress = function (method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof (method) === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('Method "' + method + '" does not exist for jQuery.autocompleteAddress');
            }
        }
    } )(window.jQuery);

    // ShippingData object
    function ShippingData() {
        var self = this;
        var src, method, lang;

        self.methods_city = [
            'novaposhta.warehouse',
            'novaposhta.doors'
        ];

        self.methods_address = [
            'novaposhta.warehouse'
        ];

        self.setProp = function () {
            self.method = $('input[name="shipping_method"]:checked').val() || $('select[name="shipping_method"]').val();

            self.lang =  $('html').attr('lang');
        }

        self.handlerChanges = function (name, value) {
            if ($.inArray(self.method, self.methods_city.concat(self.methods_address)) != - 1) {
                if (name.match(/zone/i)) {
                    $('input[name*="city"]:visible').val('');
                    $('input[name*="address_1"]:visible').val('');
                } else if (name.match(/city/i)) {
                    $('input[name*="address_1"]:visible').val('');
                } else if (name.match(/shipping\_method/i)) {
                    $('input[name*="city"]:visible').autocompleteAddress('destroy');
                    $('input[name*="address_1"]:visible').val('').autocompleteAddress('destroy');

                    self.method = value;
                }
            } else if ($.inArray(value, self.methods_city.concat(self.methods_address)) != - 1) {
                if (name.match(/shipping\_method/i)) {
                    $('input[name*="city"]:visible').val('');
                    $('input[name*="address_1"]:visible').val('');

                    self.method = value;
                }
            }
        }

        self.getAddress = function (element, search) {
            var filter, action;

            if (element[0].name.match(/city/i)) {
                action = 'getCities';
                filter = $('[name*="zone"]:visible').val() || '';
            } else if (element[0].name.match(/address_1/i)) {
                action = 'getWarehouses';
                filter = $('[name*="city"]:visible').val();
            }

            if (!search) {
                search = element[0].value;
            }

            return $.ajax( {
                url: 'index.php?route=extension/module/shippingdata/getShippingData',
                type: 'POST',
                data: 'shipping=' + self.method + '&action=' + action + '&filter=' + encodeURIComponent(filter) + '&search=' + encodeURIComponent(search),
                dataType: 'json',
                global: false,
                success: function (json) {
                    self.src = json;
                }
            } );
        }
    }

    // DOOM loaded
    $(function () {
        var shippingData = new ShippingData();

        // Settings properties after DOOM load
        shippingData.setProp();

        // Settings properties after ajaxStop
        $(document).ajaxStop(function () {
            shippingData.setProp();
        } );

        // Check changes
        $(document).on('change', '[name*="zone"]:visible, [name*="city"]:visible, [name*="shipping_method"]', function (e) {
            shippingData.handlerChanges(e.target.name, e.target.value);
        });

        // Add autocomplete for address
        $('body').on('focus', 'input[name*="city"]:visible, input[name*="address_1"]:visible', function () {
            if (this.name.match(/city/i) && $.inArray(shippingData.method, shippingData.methods_city) != - 1 || this.name.match(/address_1/i) && $.inArray(shippingData.method, shippingData.methods_address) != - 1) {
                $(this).autocompleteAddress( {
                    source: function (request, response) {
                        shippingData.getAddress(this, request).done(function () {
                            response($.map(shippingData.src, function (item) {
                                return {
                                    label: item['Description'],
                                    value: item['Description']
                                }
                            } ));
                        } );
                    },
                    select: function (e) {
                        if (e.value != this.val()) {
                            this.val(e.value).trigger('change');
                        }
                    },
                    class: 'dropdown-address'
                } );
            }
        } );
    } );
    //--></script>
<?php } ?>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>