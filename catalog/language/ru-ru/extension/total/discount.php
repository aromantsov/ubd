<?php
// Heading
$_['heading_title_incentive_program_accamulation'] = 'Накопительная скидка за общую сумму заказов более %s (%s)';
$_['heading_title_incentive_program_summ_current_order'] = 'Скидка за сумму заказа более %s (%s)';
$_['personal_discount'] = 'Персональная скидка (%s)';
$_['code'] = 'по коду';
$_['admin_discount'] = 'Скидка от администратора (%s)';
$_['result_discount'] = 'С учетом максимально допустимой скидки на товар';

