<?php
// Text
$_['text_title']       = 'Оплата по безналичному расчету';
$_['text_instruction'] = '<div class="text-center">Для того, чтобы распечатать счет нажмите на кнопку ниже (откроется в новом окне)</div>';
$_['text_payment']     = '<div class="text-center">Заказ не будет выполнен, пока деньги не поступят на наш расчетный счёт.</div>';
$_['text_order_history']  = 'Счет хранится в <a href="{href}" target="_blank">Истории заказов</a>';
$_['text_printpay']       = '<div class="text-center"><a style="display: inline-block !important;" href="{href}" class="btn btn-success" style="text-decoration:none;margin:0 auto;" target="_blank">Печать cчета</a></div>';
$_['text_loading']       = 'Загрузка';
$_['text_month_1']  =   'Січня';
$_['text_month_2']  =   'Лютого';
$_['text_month_3']  =   'Березня';
$_['text_month_4']  =   'Квітня';
$_['text_month_5']  =   'Травня';
$_['text_month_6']  =   'Червня';
$_['text_month_7']  =   'Липня';
$_['text_month_8']  =   'Серпня';
$_['text_month_9']  =   'Вересня';
$_['text_month_10']  =   'Жовтня';
$_['text_month_11']  =   'Листопада';
$_['text_month_12']  =   'Грудня';
?>