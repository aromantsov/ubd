<?php
class ControllerExtensionModuleCustomerImport extends Controller {
	public function index(){

		$this->load->language('extension/module/customer_import');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['entry_status'] = $this->language->get('entry_status');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/category', 'token=' . $this->session->data['token'], true)
		);

		if (isset($this->session->data['error'])) {
			$data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error'] = '';
		}

        if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action'] = $this->url->link('extension/module/customer_import/readfile', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

		$data['text_button'] = $this->language->get('text_button');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/customer_import', $data));

	}

	public function readfile(){

        $this->load->language('extension/module/customer_import');

	    if(empty($this->request->post['link'])){
            $this->session->data['error'] = $this->language->get('no_link');
            $this->index();
            return;
        }

		$this->load->model('extension/module/customer_import');

		$json = file_get_contents($this->request->post['link']);

		if($json){

		    $customers = json_decode($json, true);

		    if(is_array($customers)){

		        $customers = $customers['result'];

		        foreach($customers as $customer){
                    $this->model_extension_module_customer_import->setCustomer($customer);
		        }

                $this->session->data['success'] = $this->language->get('text_success');

		        $this->index();

		    }else{
			    $this->session->data['error'] = $this->language->get('no_json');
			    $this->index();
		    }
		}
	}
}