<?php
// Heading
$_['heading_title']    = 'Импорт пользователей';

// Text
$_['text_extension']   = 'Модули';
$_['text_success']     = 'Импорт успешно завершен';
$_['text_edit']        = 'Редактирование модуля';
$_['text_button']      = 'Загрузить';

// Entry
$_['entry_status']     = 'Ссылка (поддерживается только формат json)';

// Error
$_['error_permission'] = 'У вас нет прав для управления этим модулем!';
$_['no_json']          = 'Формат не json';
$_['no_link']          = 'Не указана ссылка';