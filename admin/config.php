<?php
// HTTP
define('HTTP_SERVER', 'http://ubd.loc/admin/');
define('HTTP_CATALOG', 'http://ubd.loc/');

// HTTPS
define('HTTPS_SERVER', 'http://ubd.loc/admin/');
define('HTTPS_CATALOG', 'http://ubd.loc/');

// DIR
define('DIR_APPLICATION', 'D:/openserver/domains/ubd.loc/admin/');
define('DIR_SYSTEM', 'D:/openserver/domains/ubd.loc/system/');
define('DIR_IMAGE', 'D:/openserver/domains/ubd.loc/image/');
define('DIR_LANGUAGE', 'D:/openserver/domains/ubd.loc/admin/language/');
define('DIR_TEMPLATE', 'D:/openserver/domains/ubd.loc/admin/view/template/');
define('DIR_CONFIG', 'D:/openserver/domains/ubd.loc/system/config/');
define('DIR_CACHE', 'D:/openserver/domains/ubd.loc/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/openserver/domains/ubd.loc/system/storage/download/');
define('DIR_LOGS', 'D:/openserver/domains/ubd.loc/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/openserver/domains/ubd.loc/system/storage/modification/');
define('DIR_UPLOAD', 'D:/openserver/domains/ubd.loc/system/storage/upload/');
define('DIR_CATALOG', 'D:/openserver/domains/ubd.loc/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'ubd');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
