<?php
class ControllerExtensionPaymentSoforpInvoice extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/payment/soforp_invoice');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		$this->load->model('extension/payment/soforp_invoice');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('soforp_invoice', $this->request->post);

			$this->model_extension_payment_soforp_invoice->editSoforp($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'].'&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');

		$data['supplier_info'] = $this->language->get('supplier_info');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_sheff'] = $this->language->get('entry_sheff');
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
        $data['entry_tax_status'] = $this->language->get('entry_tax_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_checking_account'] = $this->language->get('entry_checking_account');
		$data['entry_egrpou'] = $this->language->get('entry_egrpou');
		$data['entry_mfo'] = $this->language->get('entry_mfo');
		$data['entry_bank'] = $this->language->get('entry_bank');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (isset($this->error['supplier_info_' . $language['language_id']])) {
				$data['error_supplier_info_' . $language['language_id']] = $this->error['supplier_info_' . $language['language_id']];
			} else {
				$data['error_supplier_info_' . $language['language_id']] = '';
			}
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/extension', 'token=' . $this->session->data['token'].'&type=payment', true),
      		'separator' => ' :: '
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/payment/soforp_invoice', 'token=' . $this->session->data['token'], true),
      		'separator' => ' :: '
   		);

		$data['action'] = $this->url->link('extension/payment/soforp_invoice', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'].'&type=payment', true);

		foreach ($languages as $language) {
			if (isset($this->request->post['soforp_invoice_supplier_info_' . $language['language_id']])) {
				$data['soforp_invoice_supplier_info_' . $language['language_id']] = $this->request->post['soforp_invoice_supplier_info_' . $language['language_id']];
			} else {
				$data['soforp_invoice_supplier_info_' . $language['language_id']] = $this->config->get('soforp_invoice_supplier_info_' . $language['language_id']);
			}
		}

		$data['languages'] = $languages;

		$soforp_invoice_data = $this->model_extension_payment_soforp_invoice->getSoforp();

		if ($soforp_invoice_data['checking_account']) {
			$data['soforp_invoice_checking_account'] = $soforp_invoice_data['checking_account'];
		} else {
			$data['soforp_invoice_checking_account'] = '';
		}

		if ($soforp_invoice_data['bank']) {
			$data['soforp_invoice_bank'] = $soforp_invoice_data['bank'];
		} else {
			$data['soforp_invoice_bank'] = '';
		}

		if ($soforp_invoice_data['egrpou']) {
			$data['soforp_invoice_egrpou'] = $soforp_invoice_data['egrpou'];
		} else {
			$data['soforp_invoice_egrpou'] = '';
		}

		if ($soforp_invoice_data['mfo']) {
			$data['soforp_invoice_mfo'] = $soforp_invoice_data['mfo'];
		} else {
			$data['soforp_invoice_mfo'] = '';
		}

		if (isset($this->request->post['soforp_invoice_total'])) {
			$data['soforp_invoice_total'] = $this->request->post['soforp_invoice_total'];
		} else {
			$data['soforp_invoice_total'] = $this->config->get('soforp_invoice_total');
		}

		if (isset($this->request->post['soforp_invoice_sheff'])) {
			$data['soforp_invoice_sheff'] = $this->request->post['soforp_invoice_sheff'];
		} else {
			$data['soforp_invoice_sheff'] = $this->config->get('soforp_invoice_sheff');
		}

		if (isset($this->request->post['soforp_invoice_order_status_id'])) {
			$data['soforp_invoice_order_status_id'] = $this->request->post['soforp_invoice_order_status_id'];
		} else {
			$data['soforp_invoice_order_status_id'] = $this->config->get('soforp_invoice_order_status_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['soforp_invoice_geo_zone_id'])) {
			$data['soforp_invoice_geo_zone_id'] = $this->request->post['soforp_invoice_geo_zone_id'];
		} else {
			$data['soforp_invoice_geo_zone_id'] = $this->config->get('soforp_invoice_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['soforp_invoice_status'])) {
			$data['soforp_invoice_status'] = $this->request->post['soforp_invoice_status'];
		} else {
			$data['soforp_invoice_status'] = $this->config->get('soforp_invoice_status');
		}

        if (isset($this->request->post['soforp_invoice_tax_status'])) {
            $data['soforp_invoice_tax_status'] = $this->request->post['soforp_invoice_tax_status'];
        } else {
            $data['soforp_invoice_tax_status'] = $this->config->get('soforp_invoice_tax_status');
        }

		if (isset($this->request->post['soforp_invoice_sort_order'])) {
			$data['soforp_invoice_sort_order'] = $this->request->post['soforp_invoice_sort_order'];
		} else {
			$data['soforp_invoice_sort_order'] = $this->config->get('soforp_invoice_sort_order');
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/soforp_invoice', $data) );
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/soforp_invoice')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (!$this->request->post['soforp_invoice_supplier_info_' . $language['language_id']]) {
				$this->error['supplier_info_' .  $language['language_id']] = $this->language->get('error_supplier_info');
			}
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>