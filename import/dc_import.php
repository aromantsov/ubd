<?php
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(0);
require 'db_site.php';

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'http://api.ubd.ua/dc.json');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, "task=dc");
$json  = curl_exec($curl);
$discoun_cards = json_decode($json, true);

if(is_array($discoun_cards)) {
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// mysqli_query("SET NAMES utf-8");
 //    mysqli_query("SET CHARACTER SET 'utf-8'"); 
	mysqli_set_charset("utf8");
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
	$discount_cards = $discoun_cards['result'];
	foreach($discount_cards as $discount_card) {
		//mb_convert_encoding($discount_card['fio'], "UTF-8", "Windows-1251");
		$sql_customer = "SELECT customer_id, custom_field FROM oc_customer WHERE code = " . $discount_card['code'];
		$customer = mysqli_query($conn, $sql_customer);
		if (mysqli_num_rows($customer) == 0) {
			$fio = explode(' ', $discount_card['fio']);
			if(count($fio) >= 4) {
				$lastname_array = array($fio[0], $fio[1], $fio[2]);
				$lastname = implode(' ', $lastname_array);
				$name = $fio[3];
			} else {
				$lastname = $fio[0];
				if(isset($fio[1])){
					$name = $fio[1];
				} else {
					$name = '';
				}
			}
			$dc = array(
				1 => $discount_card['code'],
				2 => $discount_card['rate']
			);
			$custom_field = json_encode($dc, JSON_UNESCAPED_UNICODE);	
			$sql = "INSERT INTO oc_customer (customer_group_id, firstname, lastname, custom_field, status, code) VALUES (1, \"" . htmlentities($name) . "\", \"" . htmlentities($lastname) . "\",'" . $custom_field . "', 1, '" . $discount_card['code'] . "')";
		} else {
			$customer = mysqli_fetch_assoc($customer);
			$char = json_decode($customer['custom_field'], true);
			$char[2] = $discount_card['rate'];
			$custom_field = json_encode($char, JSON_UNESCAPED_UNICODE);
			$sql = "UPDATE oc_customer SET custom_field = '" . $custom_field . "', code = '" . $discount_card['code'] . "' WHERE customer_id = " . $customer['customer_id'];	
		}
		mysqli_query($conn, $sql);
	}
	mysqli_close($conn);
	echo "OK";
} else {
	echo "ERROR";
}

?>