<?php
// Heading
$_['heading_title']      = 'Оплата по безналу / Накладная / Фактура';

// Text
$_['text_payment']       = 'Оплата';
$_['text_success']       = 'Настройки модуля обновлены!';
$_['text_edit']       = 'Редактировать';

// Entry
$_['supplier_info']      = 'Продавец / Поставщик:';
$_['entry_sheff']        = 'Руководитель:';
$_['entry_total']        = 'Нижняя граница:';
$_['entry_order_status'] = 'Статус заказа после оплаты:';
$_['entry_geo_zone']     = 'Географическая зона:';
$_['entry_status']       = 'Статус:';
$_['entry_tax_status']   = 'Использовать НДС:';
$_['entry_sort_order']   = 'Порядок сортировки:';

// Error
$_['error_permission']   = 'У Вас нет прав для управления этим модулем!';
$_['error_supplier_info']= 'Это поле обязательно для заполнения!';
?>