<?php
class ModelExtensionModuleCustomerImport extends Model {
	public function setCustomer($user){

        $query = $this->db->query("SELECT customer_id, custom_field FROM oc_customer WHERE code = " . $user['code']);
        $customer = $query->row;
        if ($query->num_rows == 0) {
            $fio = explode(' ', $user['fio']);
            if(count($fio) >= 4) {
                $lastname_array = array($fio[0], $fio[1], $fio[2]);
                $lastname = implode(' ', $lastname_array);
                $name = $fio[3];
            } else {
                $lastname = $fio[0];
                if(isset($fio[1])){
                    $name = $fio[1];
                } else {
                    $name = '';
                }
            }
            $dc = array(
                1 => $user['code'],
                2 => $user['rate']
            );
            $custom_field = json_encode($dc, JSON_UNESCAPED_UNICODE);   
            $sql = "INSERT INTO oc_customer (customer_group_id, firstname, lastname, custom_field, status, code) VALUES (1, \"" . htmlentities($name) . "\", \"" . htmlentities($lastname) . "\",'" . $custom_field . "', 1, '" . $user['code'] . "')";
        } else {
            $char = json_decode($customer['custom_field'], true);
            $char[2] = $user['rate'];
            $custom_field = json_encode($char, JSON_UNESCAPED_UNICODE);
            $sql = "UPDATE oc_customer SET custom_field = '" . $custom_field . "', code = '" . $user['code'] . "' WHERE customer_id = " . $customer['customer_id'];    
        }
        $this->db->query($sql);    
	}
}	